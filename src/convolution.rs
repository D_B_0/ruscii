use image::{ImageBuffer, Pixel, Rgba};

pub(crate) type Kernel<'a> = &'a [&'a [f32]];

pub(crate) fn convolve(
    pix: &ImageBuffer<Rgba<f32>, Vec<f32>>,
    ker: &Kernel,
) -> ImageBuffer<Rgba<f32>, Vec<f32>> {
    assert_eq!(ker.len() % 2, 1);
    assert_eq!(ker[0].len() % 2, 1);
    for row in ker.iter() {
        assert_eq!(row.len(), ker[0].len());
    }

    let sum = match ker.iter().fold(0.0, |acc, val| {
        acc + val.iter().fold(0.0, |acc, &val| acc + val)
    }) {
        x if x == 0.0 => 1.0,
        val => val,
    };
    let y_radius = (ker.len() as isize) / 2;
    let x_radius = (ker[0].len() as isize) / 2;
    let mut new_img = ImageBuffer::new(
        pix.width() - (x_radius as u32),
        pix.height() - (y_radius as u32),
    );

    let y_range = if y_radius != 0 {
        (-y_radius)..=y_radius
    } else {
        0..=0
    };
    let x_range = if x_radius != 0 {
        (-x_radius)..=x_radius
    } else {
        0..=0
    };

    for y in (y_radius as u32)..(pix.height() - y_radius as u32) {
        for x in (x_radius as u32)..(pix.width() - x_radius as u32) {
            let mut acc = *Rgba::from_slice(&[0.0, 0.0, 0.0, 0.0]);

            for dy in y_range.clone() {
                let c_y = (dy + y as isize) as u32;
                for dx in x_range.clone() {
                    let c_x = (dx + x as isize) as u32;

                    let mut p = *pix.get_pixel(c_x, c_y);
                    p.apply(|c| c * ker[(dy + y_radius) as usize][(dx + x_radius) as usize]);
                    acc.apply2(&p, |c1, c2| c1 + c2);
                }
            }

            acc.apply(|c| c / sum);
            acc.0[3] = 1.0;
            new_img.put_pixel(x - (x_radius as u32), y - (y_radius as u32), acc);
        }
    }
    new_img
}

pub(crate) fn convolve_multiple(
    img: &ImageBuffer<Rgba<f32>, Vec<f32>>,
    kers: &[Kernel],
) -> ImageBuffer<Rgba<f32>, Vec<f32>> {
    let mut img = img.clone();
    for ker in kers {
        img = convolve(&img, ker);
    }
    img
}
