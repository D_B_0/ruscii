use std::{
    error::Error,
    f32::consts::{FRAC_PI_8, PI},
    fs::File,
    io::Write,
    path::PathBuf,
};

use clap::{Parser, Subcommand};
use colors_transform::{Color, Hsl};
use image::{ImageBuffer, ImageResult, Rgba};

mod convolution;
use convolution::*;

#[derive(Parser, Debug)]
struct Args {
    #[command(subcommand)]
    command: Commands,
}

#[derive(Subcommand, Debug)]
enum Commands {
    Edges {
        filepath: PathBuf,

        #[arg(short = 'o')]
        savepath: Option<PathBuf>,
    },
    Ascii {
        filepath: PathBuf,

        #[arg(short = 'o')]
        savepath: Option<PathBuf>,
    },
}

fn extract_data(img: &ImageBuffer<Rgba<f32>, Vec<f32>>) -> Vec<(f32, f32)> {
    let gx = convolve_multiple(img, &[&[&[1.0, 0.0, -1.0]], &[&[1.0], &[2.0], &[1.0]]]);
    let gy = convolve_multiple(img, &[&[&[1.0, 2.0, 1.0]], &[&[1.0], &[0.0], &[-1.0]]]);

    gx.pixels()
        .zip(gy.pixels())
        .map(|(px, py)| {
            let theta = f32::atan2(py.0[0], px.0[0]);
            let mag = (px.0[0] * px.0[0] + py.0[0] * py.0[0]).sqrt();
            (mag, theta)
        })
        .collect()
}

fn data_to_img(data: &[(f32, f32)], width: u32, height: u32) -> ImageBuffer<Rgba<f32>, Vec<f32>> {
    let mut new_image = ImageBuffer::new(width, height);
    for y in 0..height {
        for x in 0..width {
            let (mag, theta) = data[(x + y * width) as usize];
            let c = Hsl::from(((theta + PI) / (2.0 * PI)) * 360.0, 100.0, 50.0).to_rgb();
            let p: &mut Rgba<_> = new_image.get_pixel_mut(x, y);
            p.0[0] = mag * c.get_red() / 255.0;
            p.0[1] = mag * c.get_green() / 255.0;
            p.0[2] = mag * c.get_blue() / 255.0;
            p.0[3] = 1.0;
        }
    }
    new_image
}

fn save(img: &ImageBuffer<Rgba<f32>, Vec<f32>>, filename: &str) -> ImageResult<()> {
    let buf = img
        .iter()
        .map(|f| (f * 255.0).floor() as u8)
        .collect::<Vec<_>>();
    image::save_buffer(
        filename,
        &buf,
        img.width(),
        img.height(),
        image::ColorType::Rgba8,
    )
}

fn main() -> Result<(), Box<dyn Error>> {
    let args = Args::parse();

    match args.command {
        Commands::Edges { filepath, savepath } => {
            let pixels = image::open(filepath.clone())?
                .grayscale()
                .blur(1.0)
                .to_rgba32f();

            let data = extract_data(&pixels);

            save(
                &data_to_img(&data, pixels.width() - 1, pixels.height() - 1),
                &savepath
                    .map(|p| p.to_str().unwrap().to_owned())
                    .unwrap_or(format!(
                        "{}_edges.{}",
                        filepath.file_stem().unwrap().to_str().unwrap(),
                        filepath.extension().unwrap().to_str().unwrap()
                    )),
            )?;
        }
        Commands::Ascii { filepath, savepath } => {
            let img = image::open(filepath)?;

            // TODO: parametrization

            let pixels = img.grayscale().blur(2.0).to_rgba32f();

            let data = extract_data(&pixels);

            let w = (pixels.width() - 1) as usize;
            let h = (pixels.height() - 1) as usize;

            let n_chars_row = 80;
            assert!(n_chars_row < w);
            let char_width = w / n_chars_row;
            let char_height = char_width * 2;
            let n_chars_col = h / char_height;

            let mut canvas = vec![vec![' '; n_chars_row]; n_chars_col];

            #[allow(clippy::needless_range_loop)]
            for char_y in 0..n_chars_col {
                for char_x in 0..n_chars_row {
                    let mut mag_avg = 0.0;
                    let mut theta_avg = 0.0;
                    let mut theta_count = 0;
                    for data_y in (char_y * char_height)..((char_y + 1) * char_height) {
                        for data_x in (char_x * char_width)..((char_x + 1) * char_width) {
                            let (mag, theta) = data[data_x + data_y * w];
                            mag_avg += mag;
                            if mag > 0.025 {
                                theta_avg += theta;
                                theta_count += 1;
                            }
                        }
                    }
                    mag_avg /= (char_width * char_height) as f32;
                    theta_avg /= theta_count as f32;

                    if theta_count != 0 {
                        // ─ │ ╱ ╲

                        let VERT = '│';
                        let DIAG_DOWN = '╲';
                        let DIAG_UP = '╱';
                        let HORIZ = '─';

                        let map = vec![
                            ((-FRAC_PI_8..=FRAC_PI_8), VERT),
                            ((FRAC_PI_8..=3.0 * FRAC_PI_8), DIAG_UP),
                            ((3.0 * FRAC_PI_8..=5.0 * FRAC_PI_8), HORIZ),
                            ((5.0 * FRAC_PI_8..=7.0 * FRAC_PI_8), DIAG_DOWN),
                            ((7.0 * FRAC_PI_8..=PI), VERT),
                            ((-3.0 * FRAC_PI_8..=-FRAC_PI_8), DIAG_DOWN),
                            ((-5.0 * FRAC_PI_8..=-3.0 * FRAC_PI_8), HORIZ),
                            ((-7.0 * FRAC_PI_8..=-5.0 * FRAC_PI_8), DIAG_UP),
                            ((-PI..=-7.0 * FRAC_PI_8), VERT),
                        ];

                        for (range, ch) in map {
                            if range.contains(&theta_avg) {
                                canvas[char_y][char_x] = ch;
                                break;
                            }
                        }
                    }
                }
            }

            if let Some(savepath) = savepath {
                let mut file = File::create(savepath.clone())?;
                for row in canvas {
                    for ch in row {
                        write!(file, "{ch}")?;
                    }
                    writeln!(file)?;
                }
                println!("Printed ascii art to {savepath:?}");
            } else {
                for row in canvas {
                    for ch in row {
                        print!("{ch}");
                    }
                    println!();
                }
            }
        }
    }

    Ok(())
}
